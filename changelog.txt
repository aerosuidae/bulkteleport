---------------------------------------------------------------------------------------------------
Version: 0.1.26
Date: 07. 08. 2019
  Changes:
    - Dashed lines fade at night
    - Dashed lines configurable RGB
---------------------------------------------------------------------------------------------------
Version: 0.1.25
Date: 06. 08. 2019
  Changes:
    - Mod setting to toggle between dashed lines and electric beams. Latter can impact performance
---------------------------------------------------------------------------------------------------
Version: 0.1.24
Date: 31. 07. 2019
  Changes:
    - Mod setting to control fluid dematerializer "full" level, due to Factorio fluid physics
---------------------------------------------------------------------------------------------------
Version: 0.1.23
Date: 28. 07. 2019
  Changes:
    - Damage to either materializer or buffer applies to both evenly
  Bugfixes:
    - Catch possible nil reference manually on placing teleporter over ghost
---------------------------------------------------------------------------------------------------
Version: 0.1.22
Date: 27. 07. 2019
  Changes:
    - Two-way teleport beams
    - Improve pipette and ghost behaviour
  Bugfixes:
    - Grammar
---------------------------------------------------------------------------------------------------
Version: 0.1.21
Date: 23. 07. 2019
  Bugfixes:
    - Check for broken telporter blueprints and alert player
    - Make the input/output buffer icon in blueprints more obvious
---------------------------------------------------------------------------------------------------
Version: 0.1.20
Date: 20. 07. 2019
  Changes:
    - Filter target rematerializers by surface
    - Don't start a cycle when LuaEntity::to_be_deconstructed()
---------------------------------------------------------------------------------------------------
Version: 0.1.19
Date: 23. 06. 2019
  Bugfixes:
    - Preserve fluid temperature across teleport
---------------------------------------------------------------------------------------------------
Version: 0.1.18
Date: 11. 05. 2019
  Bugfixes:
    - Fix free space detection for rematerializers using a limited (red bar) inventory
---------------------------------------------------------------------------------------------------
Version: 0.1.17
Date: 06. 04. 2019
  Bugfixes:
    - Check for invalid entity reference when energizer destroyed separately to buffer
    - During deconstruction, don't remove the energizer sprite before buffer is emptied
---------------------------------------------------------------------------------------------------
Version: 0.1.16
Date: 31. 03. 2019
  Changes:
    - Increase energy consumption and reduce emissions
    - Increase module slots to 2
---------------------------------------------------------------------------------------------------
Version: 0.1.15
Date: 30. 03. 2019
  Bugfixes:
    - Base tech dependency renamed: electric-energy-accumulators-1 -> electric-energy-accumulators
  Changes:
    - Switched energy_sources to use new emissions_per_second_per_watt
---------------------------------------------------------------------------------------------------
Version: 0.1.14
Date: 17. 03. 2019
  Bugfixes:
    - Avoid doubling-up flying text messages
  Changes:
    - Dual queues to improve active teleporter responsiveness
---------------------------------------------------------------------------------------------------
Version: 0.1.13
Date: 17. 03. 2019
  Changes:
    - HR sprites
---------------------------------------------------------------------------------------------------
Version: 0.1.12
Date: 12. 03. 2019
  Bugfixes:
    - Check rematerializer energy state before cycle (mostly just for sanity; wasn't possible to get free shipments)
---------------------------------------------------------------------------------------------------
Version: 0.1.11
Date: 11. 03. 2019
  Bugfixes:
    - Rematerializer could fail to export items after a cycle (race condition; affected saves should auto recover)
---------------------------------------------------------------------------------------------------
Version: 0.1.10
Date: 10. 03. 2019
  Changes:
    - UPS improvement with switch to on_nth_tick
    - Raise default checks-per-second to 20
    - Teleporter working sound
---------------------------------------------------------------------------------------------------
Version: 0.1.9
Date: 09. 03. 2019
  Changes:
    - Smooth on-tick scheduling
  Bugfixes:
    - Prevent ghosts doubling up on blueprint overlay due to energizer nil collision box
---------------------------------------------------------------------------------------------------
Version: 0.1.8
Date: 08. 03. 2019
  Changes:
    - Reduce "no target..." verbosity when valid targets exist but are all busy/full
    - Show teleport direction with electic beam (optional setting, default=true)
---------------------------------------------------------------------------------------------------
Version: 0.1.7
Date: 07. 03. 2019
  Changes:
    - Module slot
    - Removed incorrect production effect
  Bugfixes:
    - Fixed base energy consumption halving
---------------------------------------------------------------------------------------------------
Version: 0.1.6
Date: 06. 03. 2019
  Changes:
    - Fluid 2000 series teleporters
    - Remove some startup settings now that two tiers are available
  Bugfixes:
    - Revert incorrect change to energizer electric usage priority (was primary, should be secondary)
---------------------------------------------------------------------------------------------------
Version: 0.1.5
Date: 04. 03. 2019
  Changes:
    - Fluid teleporters
    - Cycles now require target has free space at least 75% of shipment size
---------------------------------------------------------------------------------------------------
Version: 0.1.4
Date: 27. 02. 2019
  Changes:
    - 0.17 compat, science pack names, item prototype flags
---------------------------------------------------------------------------------------------------
Version: 0.1.3
Date: 26. 02. 2019
  Changes:
    - Floating text warning when not attached to a network
    - A blue signal makes a rematerializer high priority
    - Start work differentiating tech 2 entity graphics
---------------------------------------------------------------------------------------------------
Version: 0.1.2
Date: 26. 02. 2019
  Changes:
    - Mod settings for cycle duration, electricity usage, inventory size and checks/second (UPS)
    - Adjusted pollution values
