#!/bin/bash
# http://www.imagemagick.org/Usage/masking/#two_background

set -e

pov=$1
passes="${pov}-passes"

out="${pov}-frames"
rm -rf ${out}
mkdir -p ${out}

for i in $(seq -w 1 30); do
	convert ${passes}/pass1${i}.png ${passes}/pass2${i}.png -alpha off \
		\( -clone 0,1 -compose difference -composite \
			-separate -evaluate-sequence max -auto-level -negate \) \
		\( -clone 0,2 -fx "v==0?0:u/v-u.p{0,0}/v+u.p{0,0}" \) \
			-delete 0,1 +swap -compose Copy_Opacity -composite \
		${out}/frame${i}.png
done
