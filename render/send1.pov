#include "colors.inc"
#include "textures.inc"
#include "shapes.inc"
#include "glass.inc"
#include "metals.inc"
#include "woods.inc"
#include "stones.inc"
#include "golds.inc"
#include "electric.inc"
#include "clock.inc"
#include "concrete.inc"
#include "camera.inc"
#include "lights.inc"

object {
	difference {
		Round_Box(<-1, 0, -1>, <1, 1.0, 1>, 0.05, true)
		sphere {
			<0,1.5,0>, 1.0
			texture { Concrete }
		}
	}
	texture { Concrete }
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.9
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.85
		}
	}
	rotate -(clock*180)*y
	texture { pigment { color rgb <0, 0.6, 0> } finish { F_MetalE } }

	translate <0,1.5,0>
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.8
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.75
		}
	}
	rotate -(clock*180)*z
	texture { T_Copper_1E }
	translate <0,1.5,0>
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.7
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.65
		}
	}
	rotate -(clock*180)*x
	texture { T_Gold_1E }
	translate <0,1.5,0>
}

box {
	<-0.2,-0.2,-0.2>, <0.2,0.2,0.2>
	texture { pigment { color White filter clock } }
	translate <0,1.4,0>
}
