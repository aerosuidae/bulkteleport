#!/bin/bash
# http://www.imagemagick.org/Usage/masking/#two_background

set -e

pov=$1
pix=$2

out="${pov}-passes"
rm -rf ${out}
mkdir -p ${out}

povray -D0 +A0.0 +AM2 +J -H${pix} -W${pix} +KFI1 +KFF30 +KC DECLARE=Pass=1 +Fn +O${out}/pass1.png -I${pov}.pov
povray -D0 +A0.0 +AM2 +J -H${pix} -W${pix} +KFI1 +KFF30 +KC DECLARE=Pass=2 +Fn +O${out}/pass2.png -I${pov}.pov
