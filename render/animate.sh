#!/bin/bash
# Make a gif. Note faster antialiasing config

set -e

pov=$1
pix=$2
crop=$3

out="${pov}-animate"
rm -rf ${out}
mkdir ${out}

povray -D0 +A -H${pix} -W${pix} +KFI1 +KFF49 +KC DECLARE=Pass=1 +Fn +O${out}/frame.png -I${pov}.pov
convert -loop 0 -delay 1 ${out}/frame*png ${pov}.gif

rm -rf ${out}
