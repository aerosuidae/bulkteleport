#include "colors.inc"
#include "textures.inc"
#include "shapes.inc"
#include "glass.inc"
#include "metals.inc"
#include "woods.inc"
#include "stones.inc"
#include "golds.inc"
#include "electric.inc"
#include "clock.inc"
#include "concrete.inc"
#include "camera.inc"
#include "lights.inc"

object {
	difference {
		union {
			Round_Cylinder(<0, 0, 0>, <0, 1.0, 0>, 1.0, 0.05, true)
			object {
				Round_Box(<0, 0, -0.3535>, <1.16, 0.5, 0.3535>, 0.05, true)
				rotate <0,45,0>
			}
			object {
				Round_Box(<-0.25, 0, -0.25>, <0.25, 0.5, 0.25>, 0.05, true)
				translate <0.8,0,-0.8>
			}
			object {
				Round_Box(<-1.16, 0, -0.3535>, <0, 0.5, 0.3535>, 0.05, true)
				rotate <0,45,0>
				translate <0,0,0.2>
			}
			object {
				Round_Box(<-0.25, 0, -0.25>, <0.25, 0.5, 0.25>, 0.05, true)
				translate <-0.8,0,1.0>
			}
		}
		cylinder {
			<0,0.5,0>, <0,2.5,0> 0.85
			texture { Concrete }
		}
	}
	texture { Concrete }
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.9
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.85
		}
	}
	rotate -(clock*180)*y
	//texture { pigment { color rgb <0, 0.259, 0.145> } finish { F_MetalA } }
	texture { pigment { color Red } finish { F_MetalE } }

	translate <0,1.5,0>
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.8
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.75
		}
	}
	texture { T_Copper_1E }
	translate <0,ClockSine+1.5,0>
}

object {
	difference {
		cylinder {
			<0,-0.08,0>, <0,0.08,0>, 0.7
		}
		cylinder {
			<0,-0.1,0>, <0,0.1,0>, 0.65
		}
	}
	texture { T_Gold_1E }
	translate <0,-ClockSine+1.5,0>
}

cylinder {
	<0,0,0>, <0,0.4,0>, 0.2
	texture { pigment { color White filter 1-clock } }
	translate <0,1.2,0>
}
